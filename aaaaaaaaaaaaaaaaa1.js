{
  "configs": [
    {
      "automock": false,
      "browser": false,
      "cache": true,
      "cacheDirectory": "/tmp/jest_rs",
      "clearMocks": false,
      "coveragePathIgnorePatterns": [
        "/node_modules/"
      ],
      "detectLeaks": false,
      "detectOpenHandles": false,
      "errorOnDeprecated": false,
      "filter": null,
      "forceCoverageMatch": [],
      "globals": {},
      "haste": {
        "defaultPlatform": "ios",
        "platforms": [
          "android",
          "ios",
          "native"
        ],
        "providesModuleNodeModules": [
          "react-native"
        ]
      },
      "moduleDirectories": [
        "node_modules"
      ],
      "moduleFileExtensions": [
        "js",
        "json",
        "jsx",
        "node"
      ],
      "moduleNameMapper": [
        [
          "^React$",
          "/home/arefin/nativebase-kitchensink-arefinjune26/node_modules/react"
        ]
      ],
      "modulePathIgnorePatterns": [
        "/home/arefin/nativebase-kitchensink-arefinjune26/node_modules/react-native/Libraries/react-native/"
      ],
      "name": "cb049005088a14a442abf5ec2ff6c589",
      "prettierPath": "/home/arefin/nativebase-kitchensink-arefinjune26/node_modules/prettier/index.js",
      "resetMocks": false,
      "resetModules": false,
      "resolver": null,
      "restoreMocks": false,
      "rootDir": "/home/arefin/nativebase-kitchensink-arefinjune26",
      "roots": [
        "/home/arefin/nativebase-kitchensink-arefinjune26"
      ],
      "runner": "jest-runner",
      "setupFiles": [
        "/home/arefin/nativebase-kitchensink-arefinjune26/node_modules/regenerator-runtime/runtime.js",
        "/home/arefin/nativebase-kitchensink-arefinjune26/node_modules/react-native/jest/setup.js"
      ],
      "setupTestFrameworkScriptFile": null,
      "skipFilter": false,
      "snapshotSerializers": [],
      "testEnvironment": "/home/arefin/nativebase-kitchensink-arefinjune26/node_modules/jest-environment-node/build/index.js",
      "testEnvironmentOptions": {},
      "testLocationInResults": false,
      "testMatch": [
        "**/__tests__/**/*.js?(x)",
        "**/?(*.)+(spec|test).js?(x)"
      ],
      "testPathIgnorePatterns": [
        "/node_modules/"
      ],
      "testRegex": "",
      "testRunner": "/home/arefin/nativebase-kitchensink-arefinjune26/node_modules/jest-cli/node_modules/jest-jasmine2/build/index.js",
      "testURL": "about:blank",
      "timers": "real",
      "transform": [
        [
          "^.+\\.js$",
          "/home/arefin/nativebase-kitchensink-arefinjune26/node_modules/babel-jest/build/index.js"
        ],
        [
          "^[./a-zA-Z0-9$_-]+\\.(bmp|gif|jpg|jpeg|mp4|png|psd|svg|webp)$",
          "/home/arefin/nativebase-kitchensink-arefinjune26/node_modules/react-native/jest/assetFileTransformer.js"
        ]
      ],
      "transformIgnorePatterns": [
        "node_modules/(?!(jest-)?react-native|react-clone-referenced-element)"
      ],
      "watchPathIgnorePatterns": []
    }
  ],
  "globalConfig": {
    "bail": false,
    "changedFilesWithAncestor": false,
    "collectCoverage": false,
    "collectCoverageFrom": null,
    "coverageDirectory": "/home/arefin/nativebase-kitchensink-arefinjune26/coverage",
    "coverageReporters": [
      "json",
      "text",
      "lcov",
      "clover"
    ],
    "coverageThreshold": null,
    "detectLeaks": false,
    "detectOpenHandles": false,
    "errorOnDeprecated": false,
    "expand": false,
    "filter": null,
    "globalSetup": null,
    "globalTeardown": null,
    "listTests": false,
    "maxWorkers": 3,
    "noStackTrace": false,
    "nonFlagArgs": [],
    "notify": false,
    "notifyMode": "always",
    "passWithNoTests": false,
    "projects": null,
    "rootDir": "/home/arefin/nativebase-kitchensink-arefinjune26",
    "runTestsByPath": false,
    "skipFilter": false,
    "testFailureExitCode": 1,
    "testPathPattern": "",
    "testResultsProcessor": null,
    "updateSnapshot": "new",
    "useStderr": false,
    "verbose": null,
    "watch": false,
    "watchman": true
  },
  "version": "23.4.1"
}
