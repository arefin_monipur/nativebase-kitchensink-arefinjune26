import React, { Component } from "react";
import { ImageBackground, View, StatusBar,Platform,Dimensions} from "react-native";
import { Container, Button, H3, Text } from "native-base";

// Code related to  GoogleAnalyticsBridge

// You have access to three classes in this module:

import {
  GoogleAnalyticsTracker,
  GoogleTagManager,
  GoogleAnalyticsSettings
} from "react-native-google-analytics-bridge";




//GoogleAnalyticsBridge




import styles from "./styles";

const launchscreenBg = require("../../../assets/launchscreen-bg.png");
const launchscreenLogo = require("../../../assets/logo-kitchen-sink.png");







function a1(){
  console.log("b");
}


class Greeting extends Component {
  render() {
    return (
      <View>

        {/*    <Button
      title="Click me"
      onPress={()=> this.cat_3()}
      style={{backgroundColor:"yellow",alignSelf:"center"}}

      />*/}

        {/*<Text onPress={a1()}> tt </Text>*/}
        {/*<Text onPress={()=>a1()}>dd  </Text>*/}
        <Text onPress={() => console.log('1st')}>Hello {this.props.name}! </Text>
      </View>
    );
  }


}

class Home extends Component {
  render() {

//GoogleAnalyticsBridge




// The tracker must be constructed, and you can have multiple:
    let tracker = new GoogleAnalyticsTracker("UA-121356681-1");


    tracker.trackScreenView("Home");





// The GoogleAnalyticsSettings is static, and settings are applied across all trackers:
    GoogleAnalyticsSettings.setDispatchInterval(60);
// Setting `dryRun` to `true` lets you test tracking without sending data to GA
    GoogleAnalyticsSettings.setDryRun(true);

// GoogleTagManager is also static, and works only with one container. All functions here are Promises:



    tracker.trackEvent("testcategory", "Hello iOS");



    tracker.trackEvent("testcategory", "Hello iOS", {
      label: "notdry",
      value: 1
    });

    tracker.trackTiming("testcategory", 13000, {
      label: "notdry",
      name: "testduration"
    });

    tracker.setTrackUncaughtExceptions(true);
    tracker.trackPurchaseEvent(
      {
        id: "P12345",
        name: "Android Warhol T-Shirt",
        category: "Apparel/T-Shirts",
        brand: "Apple",
        variant: "Black",
        price: 29.2,
        quantity: 1,
        couponCode: "APPARELSALE"
      },
      {
        id: "T12345",
        affiliation: "Apple Store - Online",
        revenue: 37.39,
        tax: 2.85,
        shipping: 5.34,
        couponCode: "SUMMER2013"
      }
    );

    tracker.trackMultiProductsPurchaseEvent(
      [
        {
          id: "2224711",
          name: "Top Ilem",
          category: "Women/Kleidung/Tops/Spitzentops",
          brand: "THE label",
          variant: "rot",
          price: 39.9,
          quantity: 1
        },
        {
          id: "2224706",
          name: "Shorts Isto",
          category: "Women/Kleidung/Hosen/Shirts",
          brand: "THE label",
          variant: "grau",
          price: 59.9,
          quantity: 1
        }
      ],
      {
        id: "T12345",
        affiliation: "THE label Shop",
        revenue: 83.87,
        tax: 15.93,
        shipping: 0.0,
        couponCode: "SUMMER2016"
      }
    );

    tracker.trackException("This is an error message", false);

    tracker.trackSocialInteraction("Twitter", "Post");

    tracker.setUser("Arefin");

    tracker.allowIDFA(true);

    tracker.setAnonymizeIp(true);

    tracker.trackScreenView("Hello");

    GoogleTagManager.openContainerWithId("GTM-NZT48")
      .then(() => {
        return GoogleTagManager.registerFunctionCallTagHandler("awzm_tag", (fn, payload) => {
          console.log("test", fn, payload)
        })
      })
      .then(() => {
        return GoogleTagManager.registerFunctionCallTagHandler("some_other_tag", (fn, payload) => {
          console.log("test2", fn, payload)
        })
      })
      .then(reg => {
        console.log("Push?: ", reg);
        return GoogleTagManager.pushDataLayerEvent({
          event: "some_event",
          id: 1
        });
      })
      .then(db => {
        console.log("db: ", db);
        return GoogleTagManager.doubleForKey("db");
      })
      .catch(err => {
        console.log(err);
      });

//GoogleAnalyticsBridge




    return (
      <Container>
        <StatusBar barStyle="light-content" />
        <ImageBackground source={launchscreenBg} style={styles.imageContainer}>
          <View style={styles.logoContainer}>
            <ImageBackground source={launchscreenLogo} style={styles.logo} />
          </View>
          <View
            style={{
              alignItems: "center",
              marginBottom: 50,
              backgroundColor: "transparent"
            }}
          >
            <H3 style={styles.text}>App to showcase</H3>
            <View style={{ marginTop: 8 }} />
            <H3 style={styles.text}>NativeBase components</H3>
            <View style={{ marginTop: 8 }} />
          </View>
          <View style={{ marginBottom: 80 }}>
            <Button
              style={{ backgroundColor: "#6FAF98", alignSelf: "center" }}
              onPress={() => this.props.navigation.navigate("DrawerOpen")}
              title="Will not be visible or rendered but added as required rule by arefin"
            >
              <Text>Lets Go!</Text>
            </Button>
            <Button
              style={{ backgroundColor: "crimson", alignSelf: "center" }}
              onPress={() => this.props.navigation.navigate("Arefin")}
              title="Will not be visible or rendered but added as required rule by arefin"
            >
              <Text>Go To Arefin(custom)</Text>
            </Button>

            <Button
              style={{ backgroundColor: "#6FAF98", alignSelf: "center",marginBottom: 10 }}
              onPress={() => this.props.navigation.navigate("ArefinWebView")}
              title="Will not be visible or rendered but added as required rule by arefin"sy
            >
              <Text>Arefin WebView</Text>
            </Button>

            <Button
              /* onPress={() => console.log('2nd')}*/

              onPress={() => _cat2()}
              title="Learn More"
              color="crimson"
            >
              <Text>Device Info</Text>
            </Button>

            <Greeting name='Jaina' />
            <Greeting name='Valeera' />

          </View>
        </ImageBackground>
      </Container>
    );


  };
}


let _cat2 = () => {

  console.log("Platform OS:",Platform.OS);

  console.log("Platform OS (Version):",Platform.Version);

  // console.log("DeviceInfo.name: ",DeviceInfo.name);
  // console.log("DeviceInfo.model: ",DeviceInfo.model());

  let DeviceInfo = require('react-native-device-info');

  console.log("DeviceInfo.getModel: ",DeviceInfo.getModel());

  console.log("getDeviceName(): ",DeviceInfo.getDeviceName());


  console.log("getUniqueID(): ",DeviceInfo.getUniqueID());
  console.log("isEmulator(): ",DeviceInfo.isEmulator());

  console.log("Window Dimensions is: ",Dimensions.get("window"));

  console.log("Screen Dimensions is: ",Dimensions.get("screen"));



  console.log("a: ",122211);
  let OS_app;

  if(Platform.OS === 'ios'){
    OS_app ="ios";
  }
  else OS_app ="Android";

}

// contentComponent at src/App.js

export default Home;
