const sum = require('./sum');

test('adds 1 + 2 to equal 3', () => {
  expect(sum(1, 2)).toBe(3);
});



test('two plus two is four', () => {
  expect(2 + 2).toBe(4);
});


test('two plus two is four', () => {
  //console.log("ex(2+2): ",expect(2+2));
  // expect(2+6) means 8 toBe (4) wrong
  expect(2 + 6).toBe(8);
});



test('object assignment', () => {
  const data = {one: 1};
  data['two'] = 2;
  expect(data).toEqual({one: 1, two: 2});
});


test('adding positive numbers is not zero', () => {
  for (let a = 1; a < 10; a++) {
    for (let b = 1; b < 10; b++) {
      expect(a + b).not.toBe(0);
    }
  }
});


test('null', () => {
  const n = null;
  expect(n).toBeNull();
  expect(n).toBeDefined();
  expect(n).not.toBeUndefined();
  expect(n).not.toBeTruthy();
  expect(n).toBeFalsy();
});

test('zero', () => {
  const z = 0;
  expect(z).not.toBeNull();
  expect(z).toBeDefined();
  expect(z).not.toBeUndefined();
  expect(z).not.toBeTruthy();
  expect(z).toBeFalsy();
});


// throw new ConfigError ('you are using the wrong JDK');
function compileAndroidCode() {
  throw ('you are using the wrong JDK');
}
test('compiling android goes as expected', () => {
  expect(compileAndroidCode).toThrow();
  //expect(compileAndroidCode).toThrow(ConfigError);

  // You can also use the exact error message or a regexp
  expect(compileAndroidCode).toThrow('you are using the wrong JDK');
  expect(compileAndroidCode).toThrow(/JDK/);
});



//test.only('this will be the only test that runs', () => {
test('this will be the only test that runs', () => {
  expect(true).toBe(true);
});

test('this test will not run', () => {
  expect('A').toBe('A');
});

function forEach(items, callback) {
  for (let index = 0; index < items.length; index++) {
    callback(items[index]);
  }
}

// const mockCallback = jest.fn();
// forEach([0, 1], mockCallback);

// The mock function is called twice
// expect(mockCallback.mock.calls.length).toBe(2);

// The first argument of the first call to the function was 0
// expect(mockCallback.mock.calls[0][0]).toBe(0);

// The first argument of the second call to the function was 1
// expect(mockCallback.mock.calls[1][0]).toBe(1);

// The return value of the first call to the function was 42

// 42 error ; undefined changed to 1==1 by arefin. later undefined.
// expect(mockCallback.mock.results[0].value).toBe(undefined);


const {parseWithComments} = require('jest-docblock');

const code = `
/**
 * This is a sample
 *
 * @flow
 */
 
 console.log('Hello World!');
`;

const parsed = parseWithComments(code);

// prints an object with two attributes: comments and pragmas.
//console.log("parsed: ",parsed);

const prettyFormat = require('pretty-format');

const val = {object: {}};
val.circularReference = val;
val[Symbol('foo')] = 'foo';
val.map = new Map([['prop', 'value']]);
val.array = [-0, Infinity, NaN];

//console.log(prettyFormat(val));

