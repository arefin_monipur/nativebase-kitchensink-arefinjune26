// __tests__/Intro-test.js
import React from 'react';
import Intro from '../intro';

import renderer from 'react-test-renderer';

test('renders correctly(2)', () => {
  const tree = renderer.create(<Intro />).toJSON();
  // console.log("tree: ",tree);
  expect(tree).toMatchSnapshot();
});
