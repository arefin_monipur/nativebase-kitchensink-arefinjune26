import React, { Component } from 'react';
import {
  Container,
  Content,
  InputGroup,
  Picker,
  Input,
  Item,
  Form,
  Label,
  Button,
  Icon,
} from 'native-base';
import {
  Text,
  View,
  ToastAndroid,
  Image,
  TouchableHighlight,
  Alert,
  TextInput,
  Dimensions,
  PixelRatio,
  TouchableOpacity,
} from 'react-native';
import Header from '../utils/Header';
import DatePicker from 'react-native-datepicker';
import OrientationLoadingOverlay from 'react-native-orientation-loading-overlay';
import Meteor, { createContainer } from 'react-native-meteor';
import moment from 'moment';
import Icon1 from 'react-native-vector-icons/Entypo';
// import ImagePicker from 'react-native-image-picker';
import ImagePicker from 'react-native-image-picker';
import { RNS3 } from 'react-native-aws3';
import { optionsS3 } from '../utils/app';

import Header2 from '../utils/Header';

export default class EditProfile extends Component {
  static navigationOptions = {
    header: null,
  };

  constructor(props) {
    super(props);

    console.log(this.props);

    let user = this.props.navigation.state.params.user;

    // if(user)

    this.state = {
      submitted: false,
      failed: null,
      loading: false,

      email: user.emails[0].address,
      name: user.profile.name,
      phone: user.profile.phone,
      address: user.profile.address,
      avatarSource: null, // { uri: user.profile.filePath },
      filePath: user.profile.filePath,
    };
  }

  selectPhotoTapped() {
    const options = {
      quality: 1.0,
      maxWidth: 500,
      maxHeight: 500,
      storageOptions: {
        skipBackup: true,
      },
    };

    ImagePicker.showImagePicker(options, response => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled photo picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        let source = { uri: response.uri };

        // You can also display the image using data:
        // let source = { uri: 'data:image/jpeg;base64,' + response.data };

        this.setState({
          avatarSource: source,
        });
      }
    });
  }

  submitRequest(data) {
    console.log(data);
    // return;
    Meteor.call('user.update', data, (error, result) => {
      if (error) {
        Alert.alert(
          'Failure',
          'Profile update request failed! Please try again later.',
          [
            {
              text: 'OK',
              onPress: () => {
                this.setState({ loading: false });
                // context.props.navigation.navigate('ParmanentRules');
              },
            },
          ],
          { cancelable: false },
        );
      }
      if (result) {
        Alert.alert(
          'Success',
          'Profile updated successfully!',
          [
            {
              text: 'OK',
              onPress: () => {
                this.setState({ loading: false });
                this.props.navigation.navigate('Feed');
              },
            },
          ],
          { cancelable: false },
        );
      }
    });
  }

  handleSubmit() {
    // Create JSON object, which will be sent to the server end...
    // if (!this.state.avatarSource) {
    //   Alert.alert('Please select an image of the guest!');
    //   return;
    // }
    let data = {
      name: this.state.name,
      phone: this.state.phone,
      email: this.state.email,
      address: this.state.address,
      userId: Meteor.userId(),
      filePath: this.state.filePath,
    };

    // console.log(data);
    // return;
    this.setState({ loading: true });

    if (!this.state.avatarSource) return this.submitRequest(data);

    const file = {
      uri: this.state.avatarSource.uri,
      //"assets-library://asset/asset.PNG?id=655DBE66-8008-459C-9358-914E1FB532DD&ext=PNG",
      name: data.name + Math.random() + 'propic.png',
      type: 'image/png',
    };

    // console.log(file.uri);
    // return;

    if (!file.uri) return this.submitRequest(data);

    RNS3.put(file, optionsS3)
      .then(response => {
        console.log('response received... ', response);
        if (response.status !== 201)
          throw new Error('Failed to upload image to S3');

        data.filePath = response.body.postResponse.location;
        return this.submitRequest(data);
      })
      .catch(error => {
        console.log(error);
      });
  }

  render() {
    // console.log(this.props.navigation.state.params.user);
    console.log(this.state);
    // let user = this.props.navigation.state.params.user;
    if (this.state.loading)
      return (
        <Container>
          <Content>
            <Header2
              headerText={'Add Allowed List'}
              navigation={this.props.navigation}
            />
            <OrientationLoadingOverlay
              visible={true}
              color="black"
              indicatorSize="large"
              messageFontSize={24}
              message="Loading..."
            />
          </Content>
        </Container>
      );
    return (
      <View style={{ flex: 1, backgroundColor: 'white' }}>
        <Header
          headerText="Update Profile"
          navigation={this.props.navigation}
        />
        <Container>
          <Content>
            <View
              style={{
                marginTop: 15,
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'center',
              }}
            >
              <TouchableOpacity onPress={this.selectPhotoTapped.bind(this)}>
                <View style={[Styles.avatar, Styles.avatarContainer]}>
                  {this.state.avatarSource === null ? (
                    this.state.filePath ? (
                      <View>
                        <Image
                          style={Styles.avatar}
                          source={{ uri: this.state.filePath }}
                        />
                        <Icon1
                          name={'camera'}
                          size={50}
                          color="#8eb9ff"
                          align="right"
                          style={{
                            position: 'absolute',
                            marginTop: 100,
                            marginLeft: 100,
                          }}
                        />
                      </View>
                    ) : (
                      <Icon1
                        name={'camera'}
                        size={70}
                        color="gray"
                        align="right"
                      />
                    )
                  ) : (
                    <Image
                      style={Styles.avatar}
                      source={this.state.avatarSource}
                    />
                  )}
                </View>
              </TouchableOpacity>
            </View>

            <Form style={{ alignItems: 'center', marginTop: 10 }}>
              <Item floatingLabel>
                <Label>Name</Label>
                <Input
                  onChangeText={name => this.setState({ name })}
                  value={this.state.name}
                />
              </Item>
              <Item floatingLabel>
                <Label>Address</Label>
                <Input
                  onChangeText={address => this.setState({ address })}
                  value={this.state.address}
                />
              </Item>
              <Item floatingLabel>
                <Label>Phone Number</Label>
                <Input
                  onChangeText={phone => this.setState({ phone })}
                  value={this.state.phone}
                />
              </Item>
            </Form>
            {/* <Text style={{ marginTop: 30, marginLeft: 10 }}>
              {' '}
              Please Select Gender:
            </Text>
            <Picker
              mode="dropdown"
              placeholder="Select One"
              selectedValue={this.state.gender}
              onValueChange={gender => {
                console.log(gender);
                this.setState({ gender });
              }}
              style={{
                marginLeft: 10,
              }}
            >
              <Picker.Item label="Select Option" value="-1" />
              <Picker.Item label="Male" value="0" />
              <Picker.Item label="Female" value="1" />
            </Picker> */}
            <Form>
              {/* <Item floatingLabel>
                <Label>Note</Label>
                <Input onChangeText={note => this.setState({ note })} />
              </Item> */}
              {/* <View> */}
              <View style={{ alignItems: 'center', alignContent: 'center' }}>
                <Item>
                  <Button
                    iconRight
                    style={{
                      marginTop: 20,
                      marginBottom: 20,
                      borderRadius: 20,
                      alignContent: 'center',
                    }}
                    onPress={this.handleSubmit.bind(this)}
                  >
                    <Text style={{ color: 'white', margin: 20 }}>Update</Text>
                    <Icon name="add" />
                  </Button>
                </Item>
              </View>
              {/* </View> */}
            </Form>
          </Content>
        </Container>
      </View>
    );
  }
}

const Styles = {
  buttonStyle: {
    marginBottom: 15,
    marginTop: 30,
    height: 50,
    width: 250,
    backgroundColor: '#eeeeff',
    borderColor: '#fff',
    shadowOpacity: 0.2,
    shadowOffset: { height: 0.5, width: 1 },
    elevation: 1.5,
  },
  headSectionStyle: {
    marginTop: 10,
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'flex-start',
  },
  containerStyle: {
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'space-around',
  },
  contentStyle: {
    opacity: 0.6,
    height: 50,
    width: 250,
    borderRadius: 70,
    borderWidth: 0.4,
    borderColor: 'gray',
  },

  avatar: {
    borderRadius: 80,
    width: 160,
    height: 160,
  },
  avatarContainer: {
    borderColor: '#9B9B9B',
    borderWidth: 3 / PixelRatio.get(),
    justifyContent: 'center',
    alignItems: 'center',
  },
};
