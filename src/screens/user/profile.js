import React, { Component } from 'react';
import {
  Text,
  View,
  Image,
  ScrollView,
  AsyncStorage,
  Alert,
} from 'react-native';
import {Button, Container, Icon, Fab } from 'native-base';
import IconMI from 'react-native-vector-icons/MaterialIcons';
import IconIon from 'react-native-vector-icons/Ionicons';
import IconMIC from 'react-native-vector-icons/MaterialCommunityIcons';


// import OneSignal from 'react-native-onesignal';
import Meteor from 'react-native-meteor';

import Header from '../utils/Header';
import EditProfile from './EditProfile';
import { NOTI_TYPES } from '../utils/const_strings.js';

class Loading extends Component {
  render() {
    console.log('loading component being rendered');
    return <Text>Loading...</Text>;
  }
}

export default class ProfilePage extends Component {
  constructor(props) {
    super(props);

    let user = this.props.navigation.state.params
      ? JSON.parse(this.props.navigation.state.params.user)
      : null;
    this.state = {
      user: user,
      flatName: null,
    };
  }

  static navigationOptions = {
    header: null,
    // drawerLockMode: 'locked-closed',
    title: 'Profile',
    drawerLabel: 'Profile',
    drawerIcon: ({ tintColor }) => <Icon name="ios-contact" />,
  };

  async fetchUserServer() {
    // console.log(this.state);
    let user = Meteor.user();
    console.log(user, this.state.user);
    if (!user) this.props.navigation.navigate('LoginScreen');
    console.log(JSON.stringify(user), JSON.stringify(this.state.user));
    if (JSON.stringify(user) == JSON.stringify(this.state.user)) {
      console.log('No need to save');
    } else {
      console.log('saving user... ', JSON.stringify(user));
      try {
        await AsyncStorage.setItem('user', JSON.stringify(user), () => {
          AsyncStorage.getItem('user', (err, result) => {
            console.log(result);
          });
        });
      } catch (error) {
        // Error saving data
        console.log(error);
      }
    }
  }

  async fetchUserLocal() {
    // console.log("fetching user...");
    let context = this;
    try {
      // console.log("accessing user...");
      let value = await AsyncStorage.getItem('user');
      console.log(value);
      if (value != null) {
        // console.log(value);
        context.setState({ user: JSON.parse(value) }, () => {
          //
        });
      } else {
        // context.props.navigation.navigate('LoginScreen');
        context.fetchUserServer().bind(this);
      }
    } catch (error) {
      // Error retrieving data
    }
  }

  fetchFlat() {
    let context = this;
    Meteor.call(
      'flats.singleFlat',
      context.state.user.profile.flatId,
      (err, res) => {
        // console.log(res);
        context.setState({
          flatName: res.flatName,
        });
      },
    );
  }

  render() {
    console.log(this.state);
    if (!this.state.user) {
      this.fetchUserLocal();
      return <Loading />;
    }

    if (!this.state.flatName) {
      this.fetchFlat();
      return <Loading />;
    }

    let props = {
      navigation: this.props.navigation,
    };

    return (
      <View>
        <Header headerText="Profile" navigation={this.props.navigation} />
        <ScrollView style={{ flex: 1, backgroundColor: 'white' }}>
          {/*<Header headerText="User Profile"/>*/}
          <View style={Styles.topCardItemStyle}>
            <View style={Styles.imageViewStyle}>
              <Image
                style={Styles.imageStyle}
                source={
                  this.state.user.profile.filePath
                    ? { uri: this.state.user.profile.filePath }
                    : require('./propic.jpg')
                }
              />
            </View>
            <Text style={Styles.doctorNameStyle}>
              {this.state.user.profile.name}
            </Text>
            <Text style={Styles.doctorTitleStyle}>
              {'Flat : ' +
                (this.state.flatName ? this.state.flatName : 'Loading')}
            </Text>
          </View>

          <View
            // style={Styles.infoContainerStyle}
            style={{ flexDirection: 'column' }}
          >
            <View style={{ flex: 1, marginTop: 20 }}>
              <Text style={{ marginLeft: 10 }}>Phone Number </Text>
              <View
                style={{
                  borderWidth: 0.2,
                  borderColor: 'red',
                  marginLeft: 10,
                  marginRight: 10,
                  marginTop: 5,
                  marginBottom: 10,
                }}
              />
              <View
                style={{
                  flex: 1,
                  flexDirection: 'row',
                  // margin: 10,
                }}
              >
                <IconIon
                  name="ios-call-outline"
                  style={{
                    flex: 0.1,
                    color: 'red',
                    fontSize: 24,
                    marginLeft: 11,
                    paddingTop: 2,
                  }}
                />
                <Text
                  style={{
                    flex: 0.9,
                    fontSize: 20,
                    fontFamily: 'Roboto',
                    textAlign: 'left',
                    // color: 'red',
                  }}
                >
                  {this.state.user.profile.phone}
                </Text>
              </View>
            </View>
          </View>

          <View
            // style={Styles.infoContainerStyle}
            style={{ flexDirection: 'column' }}
          >
            <View style={{ flex: 1, marginTop: 20 }}>
              <Text style={{ marginLeft: 10 }}>Email Address </Text>
              <View
                style={{
                  borderWidth: 0.2,
                  borderColor: 'red',
                  marginLeft: 10,
                  marginRight: 10,
                  marginTop: 5,
                  marginBottom: 10,
                }}
              />
              <View
                style={{
                  flex: 1,
                  flexDirection: 'row',
                  // margin: 10,
                }}
              >
                <IconIon
                  name="ios-mail-outline"
                  style={{
                    flex: 0.1,
                    color: 'red',
                    fontSize: 25,
                    marginLeft: 10,
                    paddingTop: 3,
                  }}
                />
                <Text
                  style={{
                    flex: 0.9,
                    fontSize: 20,
                    fontFamily: 'Roboto',
                    textAlign: 'left',
                    // color: 'red',
                  }}
                >
                  {this.state.user.emails[0].address}
                </Text>
              </View>
            </View>
          </View>

          <View
            // style={Styles.infoContainerStyle}
            style={{ flexDirection: 'column' }}
          >
            <View style={{ flex: 1, marginTop: 20 }}>
              <Text style={{ marginLeft: 10 }}>Address </Text>
              <View
                style={{
                  borderWidth: 0.2,
                  borderColor: 'red',
                  marginLeft: 10,
                  marginRight: 10,
                  marginTop: 5,
                  marginBottom: 10,
                }}
              />
              <View
                style={{
                  flex: 1,
                  flexDirection: 'row',
                  // margin: 10,
                }}
              >
                <IconIon
                  name="ios-home-outline"
                  style={{
                    flex: 0.1,
                    color: 'red',
                    fontSize: 25,
                    marginLeft: 10,
                    paddingTop: 1,
                  }}
                />
                <Text
                  style={{
                    flex: 0.9,
                    fontSize: 20,
                    fontFamily: 'Roboto',
                    textAlign: 'left',
                    // color: 'red',
                  }}
                >
                  {this.state.user.profile.address}
                </Text>
              </View>
            </View>
          </View>

          {/* <View style={Styles.singleInfo}>
                <Text style = {{fontSize: 20, fontFamily: 'Roboto_medium', paddingTop: 10}}>Email</Text>
                <Text style = {{fontSize: 20, fontFamily: 'Roboto_medium', paddingTop: 10, paddingLeft: 5, paddingRight:10,}}>:</Text>
                <Text style = {{fontSize: 20, fontFamily: 'Roboto', paddingTop: 10}}>{this.state.user.emails[0].address}</Text>
              </View>
              <View style={Styles.singleInfo}>
                <Text style = {{fontSize: 20, fontFamily: 'Roboto_medium', paddingTop: 10}}>Address</Text>
                <Text style = {{fontSize: 20, fontFamily: 'Roboto_medium', paddingTop: 10, paddingLeft: 5, paddingRight:10,}}>:</Text>
                <Text style = {{fontSize: 20, fontFamily: 'Roboto', paddingTop: 10}}>{this.state.user.profile.address}</Text>
              </View>

              <View style={Styles.singleInfo}>
                <Text style = {{fontSize: 20, fontFamily: 'Roboto_medium', paddingTop: 10}}>Phone</Text>
                <Text style = {{fontSize: 20, fontFamily: 'Roboto_medium', paddingTop: 10, paddingLeft: 5, paddingRight:10,}}>:</Text>
                <Text style = {{fontSize: 20, fontFamily: 'Roboto', paddingTop: 10}}>{this.state.user.profile.phone}</Text>
              </View> */}
        </ScrollView>

        <Fab
          style={{ backgroundColor: '#5067FF' }}
          position="bottomRight"
          onPress={() => {
            console.log('onPress being called...', this.state.user);
            this.props.navigation.navigate('EditProfile', {
              user: this.state.user,
            });
          }}
        >
          <IconMI name="edit" />
        </Fab>

        {/*<Button style={Styles.addButton} onPress={() => {alert("FAB pressed!")}}>
              <Icon name="md-create"/>
            </Button>*/}
      </View>
    );
  }
}

export const Styles = {
  addButton: {
    backgroundColor: 'grey',
    borderColor: 'grey',
    borderWidth: 1,
    height: 60,
    width: 60,
    borderRadius: 30,
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    bottom: 30,
    right: 20,
    shadowColor: '#000000',
    shadowOpacity: 0.8,
    shadowRadius: 2,
    shadowOffset: {
      height: 1,
      width: 0,
    },
    elevation: 5,
  },
  doctorNameStyle: {
    fontFamily: 'Roboto_medium',
    fontSize: 20,
    color: 'grey',
    paddingTop: 10,
  },
  singleInfo: {
    marginLeft: 20,
    // alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'center',
  },
  infoContainerStyle: {
    paddingTop: 30,
    flexDirection: 'column',
    justifyContent: 'space-between',
  },
  doctorTitleStyle: {
    fontFamily: 'Roboto_medium',
    fontSize: 15,
    color: 'grey',
  },
  imageStyle: {
    height: 150,
    width: 150,
    borderRadius: 75,
    // borderWidth: 5,
    borderColor: '#FFFFFF',
  },
  imageViewStyle: {
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'space-around',
    height: 150,
    width: 150,
    borderRadius: 75,
    borderWidth: 1,
    borderColor: 'grey',
    backgroundColor: '#FFFFFF',
  },
  topCardItemStyle: {
    paddingTop: 30,
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'column',
  },
};
