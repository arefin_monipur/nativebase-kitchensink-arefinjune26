// __tests__/profile.test.js
import React from 'react';
import EditProfile from '../EditProfile';

import renderer from 'react-test-renderer';

test('renders correctly(2)', () => {
    const tree1 = renderer.create(<EditProfile />).toJSON();
    console.log("tree1: ",tree1);
    expect(tree1).toMatchSnapshot();
});
